"""gen_lab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns

from django.shortcuts import render_to_response
from django.template import RequestContext


def handler404(request, exception, template_name="404.html"):
    response = render_to_response(template_name)
    response.status_code = 404
    return response


urlpatterns = i18n_patterns(
    re_path(r'^i18n/', include('django.conf.urls.i18n')),
    path('gjalub-ad-aslnbmjs$asdykf6yg3asdasd/', admin.site.urls),
    path('', include('backend.main.urls', namespace='main')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    prefix_default_language=False
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_ROOT, document_root=settings.STATIC_URL)

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
        re_path(r'^rosetta/', include('rosetta.urls'))
    ]

admin.site.site_header = "Ultragenom Admin"
admin.site.site_title = "Ultragenom Admin Portal"
admin.site.index_title = "Ultragenom"
