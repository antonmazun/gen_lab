BACKEND:
1) python3 -m venv env
2) source env/bin/activate
3) pip install -r req.txt
4) if 'pip command not found' , try -> pip3 install -r req.txt
5) python manage.py runserver

go to browser put -> localhost:8000

FRONT:
All frontend component contains inside 'frontend' folder
1) cd frontend
2) npm i
3) npm run watch (for dev environment) and 'npm run build' - build production


