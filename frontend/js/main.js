import InfoPage from './info'
import AccessoriesPage from './accessories';
import PartnersPage from './partners';

class Root {
    constructor() {
        this.info_page = new InfoPage();
        this.accessories_page = new AccessoriesPage();
        this.partners_page = new PartnersPage();
    }

    initScrollHeader() {
        window.onscroll = function () {
            scrollHeader()
        };
        let header = document.getElementsByTagName('header')[0];
        let sticky = header.offsetTop;

        function scrollHeader() {
            if (header.classList.contains('js-not-home')) {
                return;
            }
            (window.pageYOffset > sticky) ?
                header.classList.add("scroll-header") :
                header.classList.remove("scroll-header");
        }
    }

    callBackForm() {
        let form = document.getElementById('js_form_ajax');
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            let this_ = event.target;
            let csrfToken = this_.querySelector('input[name="csrfmiddlewaretoken"]').value;
            let name = this_.querySelector('input[name="name"]').value;
            let phone = this_.querySelector('input[name="phone"]').value;
            let email = this_.querySelector('input[name="email"]').value;
            if(!this.validateForm(name, phone, email)) {return; }
            let msg = this_.querySelector('textarea[name="msg"]').value;
            let request = new XMLHttpRequest();
            let body = `name=${encodeURIComponent(name)}&phone=${encodeURIComponent(phone)}&email=${encodeURIComponent(email)}&msg=${encodeURIComponent(msg)}`;
            request.open(this_.method, this_.action, true);
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            request.setRequestHeader("X-CSRFToken", csrfToken);
            request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    let response = JSON.parse(this.response);
                    if (response.status) {
                        document.getElementById('modal-success').style.display = 'block';
                        this_.reset();
                    }

                }
            };
            request.send(body);
        })
    }
    validateForm(name, phone, email) {
        this.validateName(name);
        this.validatePhone(phone);
        this.validateEmail(email);
        return (this.validateName(name) && this.validatePhone(phone) && this.validateEmail(email));
    }
    validateName(name) {
        if (!name.replace(/\s/g, '').replace(/%20/g, '').length) {
            this.setErrors('name-form', 'name-empty');
            return false;
        }
        const nameRegExp = new RegExp(/^[\D]+$/, 'g');
        if (!nameRegExp.test(name)) {
            this.setErrors('name-form', 'name-error');
            return false;
        }
        return true;
    }
    validatePhone(phone) {
        if(!phone.replace(/\s/g, '').replace(/%20/g, '').length) {
            this.setErrors('phone-form', 'phone-empty');
            return false;
        }
        const phoneRegExp = new RegExp(/\+?\d{1,4}?[-.\s]?\(?\d{1,3}?\)?[-.\s]?\d{1,4}[-.\s]?\d{1,4}[-.\s]?\d{1,9}$/, 'g');
        if (!phoneRegExp.test(phone)) {
            this.setErrors('phone-form', 'phone-error');
            console.log('Phone error');
            return false;
        }
        return true;
    }
    validateEmail(email) {
        if(!email.replace(/\s/g, '').replace(/%20/g, '').length) {
            this.setErrors('email-form', 'email-empty');
            return false;
        }
        const emailRegExp = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'g');
        if (!emailRegExp.test(email)) {
            this.setErrors('email-form', 'email-error');
            return false;
        }
        return true;
    }
    setErrors(fieldId, errorBlockId) {
        document.getElementById(fieldId).classList.add('error-field');
        document.getElementById(errorBlockId).style.display = 'block';
    }
    checkInput(elem, errorBlockId) {
        elem.parentNode.classList.remove('error-field');
        errorBlockId.forEach(blockId => {
            document.getElementById(blockId).style.display = 'none';
        });
    }
    closeSuccessModal() {
        document.getElementById('modal-success').style.display = 'none';
    }
    initFeedbackSwiper() {
        this.swiper = new Swiper('.feedback-swiper-container', {
            grabCursor: true,
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                992: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                1200: {
                    slidesPerView: 3,
                    spaceBetween: 20
                }
            },
            // If we need pagination
            pagination: {
                el: '.feedback-pagination',
                clickable: true
            },
        })
    }
}

window.App = new Root();
