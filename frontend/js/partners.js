export default class PartnersPage {
    constructor() {}
    init() {
        this.swiper = new Swiper('.partners-swiper', {
            grabCursor: true,
            loop: true,
            // If we need pagination
            pagination: {
                el: '.partners-pagination',
                clickable: true,
                type: 'bullets',
            },
        })
    }
}
