export default class AccessoriesPage {
    constructor() {}
    init() {
        this.swiper = new Swiper('.accessories-swiper', {
            grabCursor: true,
            loop: true,
            slidesPerView: 1,
            spaceBetween: 20,




            // If we need pagination
            pagination: {
                el: '.accessories-pagination',
                clickable: true,
                type: 'bullets',
            },
        })
    }
}
