export default class InfoPage {
    constructor() {
        this.sub_cat = document.querySelectorAll('.js-sub-cat');
    }


    init() {
        this.toggleDropDown(this.sub_cat);
    }

    toggleDropDown(sub_cat_arr) {
        sub_cat_arr.forEach((elem, index) => {
            elem.addEventListener('click', (e) => {
                let this_ = e.target;
                let data_block_meta = this_.dataset.catId;
                let target_block = document.querySelector(`[data-menu="${data_block_meta}"]`);
                this_.classList.contains('open') ? this_.classList.remove('open') : this_.classList.add('open');
                target_block.classList.contains('hide') ? target_block.classList.remove('hide') : target_block.classList.add('hide');
            })
        })
    }
}
