from django.contrib import admin
from . import models
from adminsortable.admin import SortableAdmin

# Register your models here.

@admin.register(models.Category)
class CategoryAdmin(SortableAdmin):
	pass


class SubSubKeyValueInline(admin.TabularInline):
	model = models.SubSubKeyValue
	template = "admin/key_values/stacked.html"

class SubKeyValueInline(admin.TabularInline):
	model = models.SubKeyValue



@admin.register(models.SubCategory)
class SubCategoryAdmin(SortableAdmin):
	list_display = ['id', 'name', 'category']
	list_filter = ['category']
	pass

@admin.register(models.SubSubCategory)
class SubSubCategoryAdmin(SortableAdmin):
	inlines = [SubSubKeyValueInline, ]
	list_display = ['id' , 'name' , 'category' , 'subcategory' , 'status']
	list_editable = ['name' , 'status']
	list_filter = ['category' , 'subcategory']
	# prepopulated_fields = {"slug": ("name",)}

@admin.register(models.Partner)
class PartnerAdmin(SortableAdmin):
	pass

# @admin.register(models.SubSubKeyValue)
# class SubSubKeyValueAdmin(admin.ModelAdmin):
# 	pass
#
#
# @admin.register(models.SubKeyValue)
# class SubKeyValueAdmin(admin.ModelAdmin):
# 	pass


@admin.register(models.AboutUs)
class AboutUsAdmin(SortableAdmin):
	pass


@admin.register(models.TeamMember)
class TeamMemberAdmin(SortableAdmin):
	pass


@admin.register(models.Review)
class ReviewAdmin(SortableAdmin):
	pass


@admin.register(models.Service)
class ServiceAdmin(SortableAdmin):
	pass


@admin.register(models.Form)
class FormAdmin(admin.ModelAdmin):
	list_display = ['name', 'phone', 'email' , 'status' ]
	list_editable = ['status', ]
	list_filter = ['status']

@admin.register(models.Tag)
class TagAdmin(admin.ModelAdmin):
	pass