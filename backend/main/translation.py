from modeltranslation.translator import translator, TranslationOptions
from .models import Category, SubCategory, SubSubCategory,\
	SubSubKeyValue, SubKeyValue, \
	AboutUs , TeamMember , Service , Partner


class CategoryTranslate(TranslationOptions):
	fields = ('name',)
	required_languages = ('uk', 'ru', 'en')


class SubCategoryTranslate(TranslationOptions):
	fields = ('name',)
	required_languages = ('uk', 'ru', 'en')


class SubSubCategoryTranslate(TranslationOptions):
	fields = ('name', 'text')
	required_languages = ('uk', 'ru', 'en')


class SubSubKeyValueTranslate(TranslationOptions):
	fields = ('key', 'value')
	required_languages = ('uk', 'ru', 'en')


class SubKeyValueTranslate(TranslationOptions):
	fields = ('key', 'value')
	required_languages = ('uk', 'ru', 'en')

class AboutUsTranslate(TranslationOptions):
	fields = ('text',)
	required_languages = ('uk', 'ru', 'en')

class TeamMemberTranslate(TranslationOptions):
	fields = ('name', 'occupation')
	required_languages = ('uk', 'ru', 'en')


class ServiceTranslate(TranslationOptions):
	fields = ('title', 'lead' , 'content')
	required_languages = ('uk', 'ru', 'en')

class PartnerTranslate(TranslationOptions):
	fields = ('title', 'text')
	required_languages = ('uk', 'ru', 'en')


translator.register(Category, CategoryTranslate)
translator.register(SubCategory, SubCategoryTranslate)
translator.register(SubSubCategory, SubSubCategoryTranslate)
translator.register(SubKeyValue, SubKeyValueTranslate)
translator.register(SubSubKeyValue, SubSubKeyValueTranslate)
translator.register(AboutUs, AboutUsTranslate)
translator.register(TeamMember, TeamMemberTranslate)
translator.register(Service, ServiceTranslate)
translator.register(Partner, PartnerTranslate)
