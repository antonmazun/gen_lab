from django.urls import path, include
from . import views

app_name = 'main'

urlpatterns = [
	path('', views.home, name='home'),
	path('genetic-tests/', views.gen_test, name='gen_test'),
	path('genetic-test/<slug:slug>/', views.sub_category, name='info'),
	path('service/<int:pk>/', views.service, name='service'),
	path('contacts/', views.contacts, name='contacts'),
	path('search/', views.search, name='search'),
	path('accessories/', views.accessories, name='accessories'),
	path('certificates/', views.certificates, name='certificates'),
	path('partners/', views.partners, name='partners'),
]
