from django.shortcuts import render, redirect
from .models import Category, SubCategory, SubSubCategory, Service, Form
from django.http import JsonResponse
from django.conf import settings
from .sendgrid_mail import SendGridMail

from backend.static_contact.models import Accessories, CertificateText


# Create your views here.


def home(request):
    ctx = {}
    ctx['home_page'] = True
    accessories = Accessories.objects.all()
    acc_list = []
    # for acc in accessories:
    #     print('acc', acc)
    #     acc_list.append({
    #         'acc': acc,
    #         'photos': acc.get_photos()
    #     })
    ctx['acc_list'] = accessories
    ctx['text'] = CertificateText.objects.first()
    return render(request, 'main/home.html', ctx)


def gen_test(request):
    ctx = {}
    ctx['category'] = Category.objects.all()
    return render(request, 'main/gen_test.html', ctx)


def sub_category(request, slug):
    ctx = {}
    sub_cat = SubSubCategory.objects.get(slug=slug)
    ctx['info'] = sub_cat
    return render(request, 'main/category.html', ctx)


def service(request, pk):
    ctx = {}
    ctx['service'] = Service.objects.get(pk=pk)
    return render(request, 'main/service.html', ctx)


def contacts(request):
    ctx = {}
    if request.method == 'POST':
        name = request.POST.get('name')
        phone = request.POST.get('phone')
        email = request.POST.get('email')
        msg = request.POST.get('msg')
        form = Form.objects.create(name=name, phone=phone, email=email, msg=msg)
        message_from_client = """
Номер телефону : {phone} <br>\n
Ім'я: {name}<br>\n
Email: {email}<br>\n
Повідомлення: {msg}<br>\n
"""
        str_ = message_from_client.format(phone=phone, name=name, email=email, msg=msg)
        send_grid = SendGridMail(
            subject='Повідомлення з сайту ultragenom.com',
            content=str_
        )
        try:
            response = send_grid.send_()
        except Exception as e:
            print(type(e), e)
        return JsonResponse({'status': True})
    return render(request, 'main/contacts.html', ctx)


def search(request):
    ctx = {}
    q = request.GET.get('query')
    if not q:
        return redirect('main:gen_test')
    if len(q.split()) == 1:
        q = q.strip().lower()
    else:
        q = q.lower()
    result_by_sub_sub_cat = SubSubCategory.objects.filter(tags__tag__icontains=q)
    list_category = []
    for category in Category.objects.all():
        lower_case_list_name = [name.lower() for name in
                                [category.name or '', category.name_ru or '', category.name_uk or '',
                                 category.name_en or '']]
        for name_lower in lower_case_list_name:
            if q in name_lower:
                list_category.append(category)

    list_subcategory = []
    for category in SubCategory.objects.all():
        lower_case_list_name = [name.lower() for name in
                                [category.name or '', category.name_ru or '', category.name_uk or '',
                                 category.name_en or '']]
        for name_lower in lower_case_list_name:
            if q in name_lower:
                list_subcategory.append(category)

    list_analys_by_name = []
    for analys in SubSubCategory.objects.all():
        lower_case_list_name = [name.lower() for name in
                                [analys.name or '', analys.name_ru or '', analys.name_uk or '', analys.name_en or '']]
        for name_lower in lower_case_list_name:
            if q in name_lower:
                list_analys_by_name.append(analys)

    # result_by_sub_sub_cat = None
    result_by_sub_category = list(set(list_subcategory))
    result_by_category = list(set(list_category))
    if result_by_sub_sub_cat:
        print('if!!!!!')
        if list_analys_by_name:
            print('if-if!!!!!!!')
            result_by_sub_sub_cat = list(set(list(result_by_sub_sub_cat) + list(set(list_analys_by_name))))
        else:
            print('result_by_sub_sub_cat', result_by_sub_sub_cat)
            result_by_sub_sub_cat = list(set(list(result_by_sub_sub_cat)))
        ctx['result_by_sub_sub_cat'] = result_by_sub_sub_cat
    else:
        print('ELS!!!!')
        result_by_sub_sub_cat = list(list(set(list_analys_by_name)))
        ctx['result_by_sub_sub_cat'] = result_by_sub_sub_cat

    anal_in_category = [analys for analys in result_by_sub_sub_cat if analys.category]
    anal_in_sub_category = [analys for analys in result_by_sub_sub_cat if analys.subcategory]
    ctx['result_by_sub_category'] = result_by_sub_category
    ctx['result_by_category'] = result_by_category
    ctx['search'] = True
    ctx['q'] = q
    ctx['anal_in_category'] = anal_in_category
    ctx['anal_in_sub_category'] = anal_in_sub_category
    list_response = []
    for c in result_by_category:
        new_obj = {'category': {'name': c.name, 'list_analys': c.get_sub_sub_cat(), 'subcat_list': [{
            'subcat': {
                'name': elem.name,
                'list_': list(elem.get_sub_sub_cat())
            }
        } for elem in c.get_sub_cat()]}}

        list_response.append(new_obj)
    for sc in result_by_sub_category:
        new_obj = {'category': {'name': None, 'list_analys': [], 'subcat_list': []}}
        new_sub_obj = {'subcat': {'name': '', 'list_': []}}
        status = False
        for l in list_response:
            if l['category']['name'] == sc.category.name:
                status = True
        if not status:
            new_obj['category']['name'] = sc.category.name
            new_sub_obj['subcat']['name'] = sc.name
            new_sub_obj['subcat']['list_'].append(sc.get_sub_sub_cat())
            # new_obj['category']['subcat_list'].append(new_sub_obj)
            new_obj['category']['subcat_list'] = [{
                'subcat': {
                    'name': sc.name,
                    'list_': list(sc.get_sub_sub_cat())
                }
            }]
            list_response.append(new_obj)

    for elem in anal_in_category:
        new_obj = {
            'category': {
                'name': None,
                'list_analys': [],
                'subcat_list': []
            }
        }
        status = False
        for l in list_response:
            if l['category']['name'] == elem.category.name:
                old_list = list(l['category']['list_analys'])
                old_list.append(elem)
                new_obj['category']['list_analys'] = old_list
                status = True
        if not status:
            new_obj['category']['name'] = elem.category.name
            old_list = new_obj['category']['list_analys']
            old_list.append(elem)
            new_obj['category']['list_analys'] = old_list
            list_response.append(new_obj)

    for a in anal_in_sub_category:
        new_sub_obj = {
            'subcat': {
                'name': '',
                'list_': []
            }
        }
        status = False
        for l in list_response:
            sub = False
            if l['category']['name'] == a.subcategory.category.name:
                for s in l['category']['subcat_list']:
                    if s['subcat']['name'] == a.subcategory.name:
                        s['subcat']['list_'].append(a)
                        sub = True
                if not sub:
                    new_sub_obj['subcat']['name'] = a.subcategory.name
                    new_sub_obj['subcat']['list_'].append(a)
                    l['category']['subcat_list'].append(new_sub_obj)

    for l in list_response:
        old_list = l['category']['list_analys']
        l['category']['list_analys'] = list(set(old_list))
        for elem in l['category']['subcat_list']:
            subcat_old = elem['subcat']['list_']
            elem['subcat']['list_'] = list(set(subcat_old))

    ctx['list_response'] = list_response
    return render(request, 'main/gen_test.html', ctx)


def accessories(request):
    ctx = {}
    accessories = Accessories.objects.all()
    acc_list  = []
    # for acc in accessories:
    #     print('acc' , acc)
    #     acc_list.append({
    #         'acc': acc,
    #         'photos': acc.get_photos()
    #     })
    # photos = accessories.get_photos()
    # ctx['accessories'] = accessories
    # ctx['photos'] = photos if len(photos) > 1 else photos[0]
    # ctx['len_photos'] = len(photos)
    ctx['acc_list'] = accessories
    return render(request, 'main/accessories.html', ctx)


def certificates(request):
    ctx = {}
    ctx['text'] = CertificateText.objects.first()
    return render(request, 'main/certificates.html', ctx)

def partners(request):
    ctx = {}
    ctx['text'] = CertificateText.objects.first()
    return render(request, 'main/partners.html', ctx)
