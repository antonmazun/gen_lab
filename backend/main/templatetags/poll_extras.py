from django import template
from django.conf import settings
from backend.main.models import AboutUs, TeamMember, Review, Service, Partner
from backend.static_contact.models import Phone, Email, SocialLink, Address, License, Accessories

register = template.Library()


@register.simple_tag
def get_language(request):
    print('request.LANGUAGE_CODE', request.LANGUAGE_CODE)
    full_path = request.get_full_path()
    print()
    if '/ru/' in full_path or '/en/' in full_path:
        print('asdasd')
        redirect_to = request.get_full_path()[3:]
    else:
        redirect_to = full_path
    print("get_full_path " , request.get_full_path())
    return {
        'LANGUAGES': settings.LANGUAGES,
        'SELECTEDLANG': request.LANGUAGE_CODE,
        'redirect_to': redirect_to
    }


@register.simple_tag
def get_about():
    return AboutUs.objects.all()


@register.filter
def check_reversed(value):
    return 'reversed' if int(value) % 2 == 0 else ''


@register.simple_tag
def get_team():
    return TeamMember.objects.all()


@register.simple_tag
def get_reviews():
    return Review.objects.filter(status=True)


@register.simple_tag
def get_services():
    return Service.objects.all()


# static info contacts
@register.simple_tag
def get_phones():
    return Phone.objects.all()


@register.simple_tag
def get_emails():
    return Email.objects.all()


@register.simple_tag
def get_social():
    return SocialLink.objects.all()


@register.simple_tag
def get_address():
    return Address.objects.all()


@register.simple_tag
def get_partners():
    return Partner.objects.all()


@register.simple_tag
def get_license():
    return License.objects.all()


@register.simple_tag
def get_accessories():
    return Accessories.objects.first()
