from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
import uuid
from pytils.translit import slugify
from adminsortable.models import SortableMixin


# Create your models here.

class CaseInsensitiveFieldMixin:
    """
    Field mixin that uses case-insensitive lookup alternatives if they exist.
    """

    LOOKUP_CONVERSIONS = {'exact': 'iexact', 'contains': 'icontains', 'startswith': 'istartswith',
                          'endswith': 'iendswith', 'regex': 'iregex', }

    def get_lookup(self, lookup_name):
        converted = self.LOOKUP_CONVERSIONS.get(lookup_name, lookup_name)
        return super().get_lookup(converted)


class CICharField(CaseInsensitiveFieldMixin, models.CharField):
    pass


class Category(SortableMixin):
    name = CICharField(max_length=255, verbose_name='Назва категории', db_index=True)
    the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    __str__ = lambda self: '{} ({})'.format(self.name, self.id)

    def get_sub_cat(self):
        return SubCategory.objects.filter(category=self)

    def get_sub_sub_cat(self):
        return SubSubCategory.objects.filter(category=self, status=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ['the_order']


class SubCategory(SortableMixin):
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, blank=True, null=True)
    name = models.CharField(max_length=255, verbose_name='Назва под-категории')
    the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    __str__ = lambda self: '{name} {id} ---> Категория -  {cat}'.format(cat=self.category.name if self.category else '',
                                                                        name=self.name, id=self.id)

    def get_sub_sub_cat(self):
        return SubSubCategory.objects.filter(subcategory=self)

    def get_key_values(self):
        return SubKeyValue.objects.filter(cat=self)

    class Meta:
        verbose_name = 'Подкатегория'
        verbose_name_plural = 'Подкатегории'
        ordering = ['the_order']


class Tag(models.Model):
    tag = models.CharField(max_length=255, verbose_name='Ключевые слова')

    def __str__(self):
        return self.tag


class SubSubCategory(SortableMixin):
    subcategory = models.ForeignKey(SubCategory, on_delete=models.SET_NULL, blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, blank=True, null=True)
    name = models.CharField(max_length=255, verbose_name='Название анализа')
    text = RichTextUploadingField(verbose_name='Oписаниe', blank=True, null=True)
    status = models.BooleanField(default=True, verbose_name='Показывать?')
    tags = models.ManyToManyField(Tag, blank=True, null=True, verbose_name='Ключевые слова')
    the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    slug = models.SlugField(max_length=300, unique=False, blank=True, null=True)

    # __str__ = lambda self: '{} id({})'.format(self.name , self.id)

    def save(self, *args, **kwargs):
        try:
            last_index = SubSubCategory.objects.all().last().id
            print('last_index ', last_index)
            # if not self.slug:
            # print('not slug!')
            print('self.slug ', self.slug)
            if not self.slug:
                print(slugify(self.name) + str('-' + str(last_index)))
                self.slug = slugify(self.name) + str('-' + str(last_index))
        # super(SubSubCategory, self).save(*args, **kwargs)
        except Exception as e:
            print(type(e), e)
            # if not self.slug:
            self.slug = slugify(self.name)
        super(SubSubCategory, self).save(*args, **kwargs)

    #
    def __str__(self):
        if self.subcategory:
            return '{name} : Категория - {cat} ---> Подкатегория - {sub_cat}'.format(sub_cat=self.subcategory.name,
                                                                                     name=self.name,
                                                                                     cat=self.subcategory.category.name)
        elif self.category:
            return '{name} : Категория - {cat}'.format(cat=self.category.name, name=self.name)

        return self.name

    def get_key_values(self):
        return SubSubKeyValue.objects.filter(cat=self)

    class Meta:
        verbose_name = 'Анализ'
        verbose_name_plural = 'Анализы'
        ordering = ['the_order']


class SubSubKeyValue(models.Model):
    cat = models.ForeignKey(SubSubCategory, on_delete=models.CASCADE)
    key = models.CharField(max_length=255, verbose_name='Назва')
    value = models.TextField(max_length=1500, verbose_name='Опис')

    def __str__(self):
        return f'{self.key} --- {self.value}'

    class Meta:
        verbose_name = 'Інфо. стрічка значень'
        verbose_name_plural = 'Інфо. стрічки значень'


class SubKeyValue(models.Model):
    cat = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    key = models.CharField(max_length=255)
    value = models.TextField(max_length=1500)


class AboutUs(SortableMixin):
    text = RichTextField()
    image = models.ImageField(upload_to='about_image/', blank=True, null=True)
    the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    def __str__(self):
        return self.text[:50] + '...' if len(self.text) > 50 else self.text

    class Meta:
        verbose_name = 'Блок о нас'
        verbose_name_plural = 'Блоки о нас'
        ordering = ['the_order']


class TeamMember(SortableMixin):
    photo = models.ImageField(upload_to='team/')
    name = models.CharField(max_length=255, verbose_name='Імʼя')
    occupation = models.CharField(max_length=500, verbose_name='Посада')
    the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    def __str__(self):
        return f'{self.name} {self.occupation}'

    class Meta:
        verbose_name = 'Участник команды'
        verbose_name_plural = 'Участники команды'
        ordering = ['the_order']

class Review(SortableMixin):
    photo = models.ImageField(upload_to='review_ava/', blank=True, null=True)
    name = models.CharField(max_length=255, verbose_name='Імʼя', blank=True, null=True)
    text = models.TextField(max_length=1000, verbose_name='Текст отзиву', blank=True, null=True)
    status = models.BooleanField(default=True, verbose_name='Показувати на сайті')
    the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    def __str__(self):
        return f'{self.name} {self.text[:30] + "... " if len(self.text) > 30 else self.text}'

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
        ordering = ['the_order']


class Service(SortableMixin):
    icon = models.ImageField(upload_to='service_icons/', blank=True, null=True)
    title = models.CharField(max_length=255, verbose_name='Заголовок')
    lead = models.TextField(verbose_name='Краткое описание (на главной странице)', max_length=1000)
    content = RichTextUploadingField(blank=True,
                                     null=True,
                                     config_name='special',
                                     external_plugin_resources=[
                                         ('youtube',
                                          '/static/ckeditor_plugin/ckeditor_plugins/youtube/youtube/',
                                          'plugin.js',
                                          )
                                     ], )
    the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    # static/ckeditor/ckeditor_plugins/youtube/youtube/plugin.js
    def __str__(self):
        return self.title + ' ' + self.lead

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'
        ordering = ['the_order']

class Form(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя человека', blank=True, null=True)
    phone = models.CharField(max_length=255, verbose_name='Номер человека', blank=True, null=True)
    email = models.CharField(max_length=255, verbose_name='email человека', blank=True, null=True)
    msg = models.TextField(max_length=1500, verbose_name='Сообщение', blank=True, null=True)
    status = models.BooleanField(default=False, verbose_name='Заявка обработана')

    def __str__(self):
        return self.name + ' ' + self.phone

    class Meta:
        verbose_name = 'Форма обратной связи'
        verbose_name_plural = 'Формы обратной связи'


class Partner(SortableMixin):
    img = models.ImageField(upload_to='partners/')
    link = models.URLField(verbose_name='Ссылка на партнеров', blank=True, null=True)
    title = models.CharField(max_length=255, verbose_name='Назва клініки', blank=True, null=True, default='')
    text = RichTextUploadingField(verbose_name='Текст', default='', blank=True, null=True)
    photo = models.ImageField(upload_to='partners/photos/', blank=True, null=True)
    the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    def __str__(self):
        return self.link

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'
        ordering = ['the_order']
