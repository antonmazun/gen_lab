from django.contrib import admin
from . import models
from adminsortable.admin import SortableAdmin


# Register your models here.
@admin.register(models.Phone)
class PhoneAdmin(SortableAdmin):
    pass


@admin.register(models.Email)
class EmailAdmin(admin.ModelAdmin):
    pass


@admin.register(models.SocialLink)
class SocialLinkAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Address)
class AddressAdmin(admin.ModelAdmin):
    pass


@admin.register(models.License)
class LicenseAdmin(admin.ModelAdmin):
    pass


@admin.register(models.CertificateText)
class CertificateTextAdmin(SortableAdmin):
    pass


class PhotoInline(admin.TabularInline):
    model = models.Photo


@admin.register(models.Accessories)
class AccessoriesAdmin(SortableAdmin):
    # inlines = [PhotoInline]
    # pass
    # fields = ( 'image_tag', )
    pass