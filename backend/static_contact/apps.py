from django.apps import AppConfig


class StaticContactConfig(AppConfig):
    name = 'static_contact'
