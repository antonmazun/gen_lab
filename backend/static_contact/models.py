from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from adminsortable.models import SortableMixin


# Create your models here.

class Phone(SortableMixin):
    phone = models.CharField(max_length=255, verbose_name='Телефон')
    the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    def __str__(self):
        return self.phone

    class Meta:
        verbose_name = 'Телефон'
        verbose_name_plural = 'Телефон'
        ordering = ['the_order']


class Email(models.Model):
    email = models.CharField(max_length=255, verbose_name='Емейл')

    class Meta:
        verbose_name = 'Емейл'
        verbose_name_plural = 'Емейл'


class SocialLink(models.Model):
    # icon = models.ImageField(upload_to='icons_social/', verbose_name='Иконка соц. сети' , blank=True, null=True)
    name = models.CharField(max_length=255, verbose_name='Название соц сети')
    link = models.URLField(max_length=1500, verbose_name='Ссылка на соц. сеть')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Соц. мережа'
        verbose_name_plural = 'Соц. мережі'


class Address(models.Model):
    address = models.CharField(max_length=1000, verbose_name='Адрес')

    def __str__(self):
        return self.address

    class Meta:
        verbose_name = 'Адреса'
        verbose_name_plural = 'Адреси'


class License(models.Model):
    license = models.CharField(max_length=300, default='Ліцензія №0703/06-М от 07.03.2017')
    image = models.ImageField(upload_to='license/', blank=True, null=True)
    __str__ = lambda self: self.license

    class Meta:
        verbose_name = 'Ліцензія'
        verbose_name_plural = 'Ліцензії'


class Accessories(SortableMixin):
    title = models.CharField(max_length=500, verbose_name='Заголовок страницы', default='', blank=True, null=True)
    # text = RichTextUploadingField(verbose_name='Описание страницы')
    the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    image = models.ImageField(upload_to='media/', blank=True, null=True)

    __str__ = lambda self: self.title

    # def get_photos(self):
    #     photos = Photo.objects.filter(accessories=self)
    #     return photos

    # def image_tag(self):
    #     from django.utils.html import escape
    #     return '<img src="{}" />'.format(self.image)

    # image_tag.short_description = 'Image'
    # image_tag.allow_tags = True

    class Meta:
        verbose_name = 'обладнання'
        verbose_name_plural = 'обладнання'
        ordering = ['the_order']


class Photo(models.Model):
    image = models.ImageField(upload_to='media/', blank=True, null=True)
    accessories = models.ForeignKey(Accessories, on_delete=models.CASCADE, blank=True, null=True, default='')
    # __str__ = lambda self: self


class CertificateText(SortableMixin):
    title = models.CharField(max_length=500, verbose_name='Заголовок блоку "Контроль якості"')
    text = RichTextUploadingField(verbose_name='Опис блоку "Контроль якості"')
    the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    class Meta:
        verbose_name = 'Контроль якості'
        verbose_name_plural = 'Контроль якості'
        ordering = ['the_order']
