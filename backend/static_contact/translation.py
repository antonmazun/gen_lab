from modeltranslation.translator import translator, TranslationOptions
from .models import Address, License, Accessories, CertificateText


class AddressTranslate(TranslationOptions):
    fields = ('address',)
    required_languages = ('uk', 'ru', 'en')


class LicenseTranslate(TranslationOptions):
    fields = ('license',)
    required_languages = ('uk', 'ru', 'en')


class AccessoriesTranslate(TranslationOptions):
    # fields = ('title', 'text')
    # required_languages = ('uk', 'ru', 'en')
    pass

class CertificateTextTranslate(TranslationOptions):
    fields = ('title', 'text')
    required_languages = ('uk', 'ru', 'en')


translator.register(Address, AddressTranslate)
translator.register(License, LicenseTranslate)
translator.register(Accessories, AccessoriesTranslate)
translator.register(CertificateText, CertificateTextTranslate)
